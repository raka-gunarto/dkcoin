﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Blockchain_Proof_of_Concept.Properties;

namespace Blockchain_Proof_of_Concept
{
    public partial class MainGUI : Form
    {
        private bool _isMouseDown; /* Is the user holding the left mouse button down? */
        private bool _miningButtonActive; /* Is the user currently mining? */
        private Point _prevLocation; /* Screen position of the last mouse event. */
        private bool _walletInterface; /* Is the wallet interface currently being shown? */

        public MainGUI()
        {
            InitializeComponent();

            _walletInterface = true;
            _miningButtonActive = false;
            WalletTabButton.BackgroundImage =
                Resources.WalletButtonACTIVE;
            MinerTabButton.BackgroundImage =
                Resources.MinerButtonV1PNGV2;
            foreach (Control c in Controls)
                if (c.Tag != null)
                {
                    if (c.Tag.ToString() == "MINER")
                        c.Visible = false;
                    else if (c.Tag.ToString() == "WALLET")
                        c.Visible = true;
                }

            BalanceLabelOther.Text = "";
        }

        /* Make a borderless window draggable. */
        private void GUI_MouseDown(object sender, MouseEventArgs e)
        {
            _isMouseDown = true;
            _prevLocation = e.Location;
        }

        private void GUI_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isMouseDown)
            {
                Location = new Point(Location.X - _prevLocation.X + e.X,
                    Location.Y - _prevLocation.Y + e.Y);
                Update();
            }
        }

        private void GUI_MouseUp(object sender, MouseEventArgs e)
        {
            _isMouseDown = false;
        }

        private void MinerTabButton_Click(object sender, EventArgs e)
        {
            _walletInterface = false;
            /* When the miner tab button is clicked, hide
             * anything that is tagged as part of the
             * wallet interface, and show anything
             * that is part of the miner interface. */
            foreach (Control c in Controls)
                if (c.Tag != null)
                {
                    if (c.Tag.ToString() == "WALLET")
                        c.Visible = false;
                    else if (c.Tag.ToString() == "MINER")
                        c.Visible = true;
                }

            WalletTabButton.BackgroundImage =
                Resources.WalletButtonPNGv1;
            MinerTabButton.BackgroundImage =
                Resources.MinerButtonACTIVE;
        }

        private void WalletTabButton_Click(object sender, EventArgs e)
        {
            _walletInterface = true;
            //Console.WriteLine("wallet tab button clicked");
            /* When the wallet tab button is clicked, hide
             * anything that is tagged as part of the
             * miner interface, and show anything that
             * is part of the wallet interface. */
            foreach (Control c in Controls)
                if (c.Tag != null)
                {
                    if (c.Tag.ToString() == "MINER")
                        c.Visible = false;
                    else if (c.Tag.ToString() == "WALLET")
                        c.Visible = true;
                }

            WalletTabButton.BackgroundImage =
                Resources.WalletButtonACTIVE;
            MinerTabButton.BackgroundImage =
                Resources.MinerButtonV1PNGV2;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            /* Terminate the application gracefully. */
            Application.Exit();
        }

        /* UNUSED. */

        private void MinimiseButton_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void GUI_Load(object sender, EventArgs e)
        {
        }

        private void MineButtonEnter(object sender, EventArgs e)
        {
            if (!_miningButtonActive)
                MineButton.BackgroundImage =
                    Resources.MinerPlayButton1;
            else
                MineButton.BackgroundImage =
                    Resources.MinerStopButtonHighlighted;
        }

        private void MineButtonLeave(object sender, EventArgs e)
        {
            if (!_miningButtonActive)
                MineButton.BackgroundImage =
                    Resources.MinerPlayButton2;
            else
                MineButton.BackgroundImage =
                    Resources.MinerStopButton;
        }

        private void WalletTabButton_Enter(object sender, EventArgs e)
        {
            if (!_walletInterface)
                WalletTabButton.BackgroundImage =
                    Resources.WalletButtonPNGClicked;
        }

        private void WalletTabButton_Leave(object sender, EventArgs e)
        {
            if (!_walletInterface)
                WalletTabButton.BackgroundImage =
                    Resources.WalletButtonPNGv1;
        }

        private void MinerTabButton_Enter(object sender, EventArgs e)
        {
            if (_walletInterface)
                MinerTabButton.BackgroundImage =
                    Resources.MinerButtonV1PNGV2Clicked1;
        }

        private void MinerTabButton_Leave(object sender, EventArgs e)
        {
            if (_walletInterface)
                MinerTabButton.BackgroundImage =
                    Resources.MinerButtonV1PNGV2;
        }

        /* Called when the big 'play' button on the miner interface is pressed. */
        private void MineButton_Click(object sender, EventArgs e)
        {
            /* Start or stop mining. */
            if (!_miningButtonActive)
            {
                _miningButtonActive = true;
                Program.IsMiner = true;
                Task.Run(Program.MiningLoop);
                UtilityFunctions.Print("Starting the miner...", UtilityFunctions.PRINT_TYPE.INFO);
                MineButton.BackgroundImage =
                    Resources.MinerStopButtonHighlighted;
            }
            else
            {
                _miningButtonActive = false;
                Program.IsMiner = false;
                UtilityFunctions.Print("Stopping the miner...", UtilityFunctions.PRINT_TYPE.INFO);
                MineButton.BackgroundImage =
                    Resources.MinerPlayButton1;
            }
        }

        private void ClearTransactionButton_Click(object sender, EventArgs e) // clear input boxes
        {
            TransactionAmountInput.Clear();
            TransactionTargetAddressInput.Clear();
        }

        private void SendTransactionButton_Click(object sender, EventArgs e)
        {
            string fromAddr = Program.RSAProvider.ToXmlString(false); // rsa public key
            string toAddr = TransactionTargetAddressInput.Text;

            if (float.TryParse(TransactionAmountInput.Text, out float value))
            {
                if (!new Transaction(fromAddr, toAddr, value, 0).SendTransaction())
                    MessageBox.Show("Invalid transaction parameters!", "TX Error", MessageBoxButtons.OK);

                return;
            }

            MessageBox.Show("Invalid transaction parameters!", "TX Error", MessageBoxButtons.OK);
        }

        public void UpdatePendingTransactions(ICollection<Transaction> transactions)
        {
            if (PendingTransactionList.InvokeRequired) // thread safety
            {
                Invoke(new UpdatePendingTransactionsCallback(UpdatePendingTransactions), transactions);
                return;
            }

            IEnumerable<string> txstrings = transactions.Select(i => i.ToString());

            PendingTransactionList.Items.Clear();
            PendingTransactionList.Items.AddRange(txstrings.ToArray());
        }

        public void UpdateBalance(float balance)
        {
            if (PendingTransactionList.InvokeRequired) // thread safety
            {
                Invoke(new UpdateBalanceCallback(UpdateBalance), balance);
                return;
            }

            BalanceLabel.Text = $"Balance: {balance.ToString()}";
        }

        private void BlockchainIDInput_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Block b = Program.LocalBlockchain.Chain[int.Parse(BlockchainIDInput.Text)];

                BlockchainExplorer.Items.Clear();
                BlockchainExplorer.Items.Add($"ID: {b.ID}");
                BlockchainExplorer.Items.Add($"Hash: {b.CurrentHash}");
                BlockchainExplorer.Items.Add($"Previous Block Hash: {b.PreviousHash}");
                BlockchainExplorer.Items.Add($"Timestamp: {b.TimeStamp}");
                BlockchainExplorer.Items.Add($"Transactions: {b.Transactions.Count}");

                foreach (Transaction tx in b.Transactions) BlockchainExplorer.Items.Add(tx.ToString());
            }
            catch (Exception)
            {
                BlockchainExplorer.Items.Clear();
                BlockchainExplorer.Items.Add("Invalid ID");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TransactionHistoryBox.Items.Clear();
                string addr = textBox1.Text;
                BalanceLabelOther.Text = "Balance: N/A";
                float cBalance = 0;

                foreach (Block b in Program.LocalBlockchain.Chain)
                foreach (Transaction tx in b.Transactions)
                {
                    if (tx.FromAddress == addr)
                    {
                        TransactionHistoryBox.Items.Add($"Sent {tx.Value} to {tx.ToAddress}");
                        cBalance -= tx.Value;
                    }

                    if (tx.ToAddress == addr)
                    {
                        TransactionHistoryBox.Items.Add($"Received {tx.Value} from {tx.FromAddress}");
                        cBalance += tx.Value;
                    }
                }

                if (cBalance != 0) BalanceLabelOther.Text = $"Balance: {cBalance}";
            }
            catch (Exception)
            {
                TransactionHistoryBox.Items.Clear();
                TransactionHistoryBox.Items.Add("Query failed");
                BalanceLabelOther.Text = "Balance: N/A";
            }
        }

        private delegate void UpdatePendingTransactionsCallback(ICollection<Transaction> transactions);

        private delegate void UpdateBalanceCallback(float balance);
    }
}