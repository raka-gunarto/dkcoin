﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Blockchain_Proof_of_Concept
{
    public class BlockchainNetworkListener
    {
        public TcpListener Listener;

        public event Action<Transaction> TransactionBroadcastReceived;
        public event Action<IPAddress, Block> BlockBroadcastReceived;

        /// <summary>
        /// Gets the BlockchainNetworkPacket JSON string in the request
        /// </summary>
        /// <param name="client">Client node</param>
        /// <returns>BlockchainNetworkPacket JSON string</returns>
        private string GetClientData(TcpClient client)
        {
            NetworkStream data = client.GetStream();

            List<byte> bytes = new List<byte>();
            byte b;
            do
            {
                b = (byte)data.ReadByte();
                if (b == BlockchainNetworkClient.END_OF_DATA) break;
                bytes.Add(b);
            }
            while (b != BlockchainNetworkClient.END_OF_DATA);
            return UtilityFunctions.BytesToString(bytes.ToArray<byte>());
        }

        /// <summary>
        /// Listen loop for incoming connections
        /// </summary>
        /// <returns></returns>
        public async Task ListenLoop()
        {
            while (Program.ListenForConnections)
            {

                TcpClient client = await Listener.AcceptTcpClientAsync();
                String clientData = GetClientData(client);
                IPAddress clientAddr = ((IPEndPoint)client.Client.RemoteEndPoint).Address;
                BlockchainNetworkClient.Peers.Add(clientAddr);

                UtilityFunctions.Print("Connection received from: " + clientAddr, UtilityFunctions.PRINT_TYPE.DEBUG);

                BlockchainNetworkPacket datapacket = JsonConvert.DeserializeObject<BlockchainNetworkPacket>(clientData);
                switch (datapacket.flag)
                {
                    case BlockchainNetworkPacket.TYPE_FLAGS.DISCOVER:
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        Task.Run(() => BlockchainNetworkClient.SendNodes(client, BlockchainNetworkClient.Peers.ToList()));
                        break;
                    case BlockchainNetworkPacket.TYPE_FLAGS.FULLDATA_REQ:
                        Task.Run(() => BlockchainNetworkClient.SendBlockchain(client));
                        break;
                    case BlockchainNetworkPacket.TYPE_FLAGS.DATA_REQ:
                        Task.Run(async () =>
                        {
                            if (datapacket.BlockIDRequest != null)
                                BlockchainNetworkClient.SendBlock(client, datapacket.BlockIDRequest.Value);
                            else
                            {
                                client.GetStream().WriteByte(BlockchainNetworkClient.ERROR);
                                await Task.Delay(50);
                                client.Close();
                                client.Dispose();
                            }

                        });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        break;
                    case BlockchainNetworkPacket.TYPE_FLAGS.TX_BROAD:
                        TransactionBroadcastReceived?.Invoke(datapacket.Tx);
                        client.GetStream().WriteByte(BlockchainNetworkClient.ACK);
                        await Task.Delay(50);
                        client.Close();
                        client.Dispose();
                        break;
                    case BlockchainNetworkPacket.TYPE_FLAGS.BLOCK_BROAD:
                        BlockBroadcastReceived?.Invoke(clientAddr, datapacket.Block);
                        client.GetStream().WriteByte(BlockchainNetworkClient.ACK);
                        await Task.Delay(50);
                        client.Close();
                        client.Dispose();
                        break;
                    default:
                        client.Close();
                        client.Dispose();
                        break;
                }
            }
        }

        /// <summary>
        /// BlockchainNetworkListener class.
        /// Creates a network listener to receive with other nodes' requests in the network
        /// </summary>
        public BlockchainNetworkListener()
        {
            Listener = new TcpListener(IPAddress.Any, Program.SERVER_PORT);
            Listener.Start();
        }
    }

    /// <summary>
    /// The struct sent when requesting or broadcasting information to other nodes
    /// </summary>
    public struct BlockchainNetworkPacket
    {
        // ReSharper disable InconsistentNaming
        public enum TYPE_FLAGS { DISCOVER, FULLDATA_REQ, DATA_REQ, TX_BROAD, BLOCK_BROAD } // data type
        public TYPE_FLAGS flag;
        // ReSharper restore InconsistentNaming

        public int? BlockIDRequest; // ID of requested block

        public Transaction Tx; // broadcasted transaction
        public Block Block; // broadcasted block

        public List<IPAddress> Nodes; // full list of connected peers
    }
}
