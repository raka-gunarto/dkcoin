﻿using System;
using System.Threading.Tasks;

namespace Blockchain_Proof_of_Concept
{
    partial class Program
    {
        [Obsolete("CLI is deprecated, use MainGUI")]
        public static void ShowHelp()
        {
            Console.WriteLine("SendTX");
            Console.WriteLine("ViewBalance");
            Console.WriteLine("ViewBlockchain");
            Console.WriteLine("StartMining");
            Console.WriteLine("StopMining");
        }

        [Obsolete("CLI is deprecated, use MainGUI")]
        public static void SendTx()
        {
            Console.WriteLine("Enter Address: ");
            string address = Console.ReadLine();

            Console.WriteLine("Enter Value: ");
            string val = Console.ReadLine();

            Transaction tx = new Transaction(RSAProvider.ToXmlString(false), address, float.Parse(val), 0);
            tx.SendTransaction();
        }

        [Obsolete("CLI is deprecated, use MainGUI")]
        public static void UserInputLoop()
        {
            while (true)
            {
                Console.Write("DKCoin> ");
                String input = Console.ReadLine();
                switch (input)
                {
                    case "SendTX":
                        SendTx();
                        break;
                    case "ViewBalance":
                        UtilityFunctions.Print("Balance: " + LocalBlockchain.GetAddressBalance(RSAProvider.ToXmlString(false)), UtilityFunctions.PRINT_TYPE.INFO);
                        break;
                    case "ViewBlockchain":
                        foreach(Block b in LocalBlockchain.Chain)
                        {
                            UtilityFunctions.Print("Block: " + b.ID, UtilityFunctions.PRINT_TYPE.INFO);
                            UtilityFunctions.Print("Hash: " + b.CurrentHash, UtilityFunctions.PRINT_TYPE.INFO);
                            UtilityFunctions.Print("Previous Hash: " + b.PreviousHash, UtilityFunctions.PRINT_TYPE.INFO);
                            UtilityFunctions.Print("Timestamp: " + b.TimeStamp, UtilityFunctions.PRINT_TYPE.INFO);
                            UtilityFunctions.Print("Nonce: " + b.Nonce, UtilityFunctions.PRINT_TYPE.INFO);
                            UtilityFunctions.Print("Transactions:", UtilityFunctions.PRINT_TYPE.INFO);

                            foreach(Transaction tx in b.Transactions)
                            {
                                UtilityFunctions.Print("From: " + tx.FromAddress, UtilityFunctions.PRINT_TYPE.INFO);
                                UtilityFunctions.Print("To: " + tx.ToAddress, UtilityFunctions.PRINT_TYPE.INFO);
                                UtilityFunctions.Print("Value: " + tx.Value, UtilityFunctions.PRINT_TYPE.INFO);
                                Console.WriteLine("");
                            }

                            Console.WriteLine("");
                        }
                        break;
                    case "StartMining":
                        if (IsMiner)
                        {
                            UtilityFunctions.Print("Miner already started!", UtilityFunctions.PRINT_TYPE.INFO);
                            break;
                        }
                        UtilityFunctions.Print("Starting the Miner", UtilityFunctions.PRINT_TYPE.INFO);
                        IsMiner = true;
                        Task.Run(MiningLoop);
                        break;
                    case "StopMining":
                        UtilityFunctions.Print("Stopping the Miner", UtilityFunctions.PRINT_TYPE.INFO);
                        IsMiner = false;
                        break;
                    case "help":
                        ShowHelp();
                        break;
                    default:
                        Console.WriteLine("Invalid command!");
                        break;
                }
            }
        }
    }
}
