﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Blockchain_Proof_of_Concept
{
    public class Transaction : IComparable
    {
        public string FromAddress;
        public string ToAddress;
        public byte[] Signature;
        public int TimeStamp;
        public float Value;
        public float MinerReward;

        /// <summary>
        /// Transaction class.
        /// Represents a transaction in the blockchain
        /// </summary>
        /// <param name="fromAddr">RSA public key of the sender</param>
        /// <param name="toAddr">RSA public key of the recipient</param>
        /// <param name="val">Value sent</param>
        /// <param name="minerReward">Reward given to the miner who includes this transaction in their next block (currently not implemented)</param>
        public Transaction(string fromAddr, string toAddr,
                            float val, float minerReward)
        {
            FromAddress = fromAddr;
            ToAddress = toAddr;
            TimeStamp = UtilityFunctions.GetUnixTime();
            Value = val;
            MinerReward = minerReward;
        }

        public override bool Equals(object obj)
        {
            Transaction othertx = (Transaction)obj;
            if (othertx == null) return false;
            if (othertx.Signature == null && Signature == null) return true;
            if (othertx.Signature == null && Signature != null) return false;
            if (othertx.Signature != null && Signature == null) return false;
            if (Signature.Length != othertx.Signature.Length) return false;
            for (int i = 0; i < Signature.Length; i++)
            {
                if (Signature[i] != othertx.Signature[i]) return false;
            }
            return true;
        }

        public int CompareTo(object obj)
        {
            Transaction otherTx = (Transaction) obj;
            return MinerReward.CompareTo(otherTx.MinerReward);
        }

        public override string ToString()
        {
            return String.Format("{0} sends {1} DKCoin to {2}", FromAddress, Value, ToAddress);
        }

        /// <summary>
        /// Broadcasts the transaction to the network
        /// </summary>
        /// <returns>Flag to indicate broadcast success/failure</returns>
        public bool SendTransaction()
        {
            float senderBalance = Program.LocalBlockchain.GetAddressBalance(FromAddress);
            if (senderBalance >= Value)
            {
                Signature = Program.RSAProvider.SignData(Encoding.ASCII.GetBytes(FromAddress + " " + ToAddress + " " + TimeStamp + " " + Value + " " + MinerReward), new SHA1CryptoServiceProvider());

                Program.LocalBlockchain.PendingTransactions.Add(this);
                Program.MainGuiWindow.UpdatePendingTransactions(Program.LocalBlockchain.PendingTransactions);

                BlockchainNetworkClient.TransactionBroadcast(this);
                return true;
            }

            UtilityFunctions.Print("Not enough funds! Aborting transaction", UtilityFunctions.PRINT_TYPE.ERROR);
            return false;
        }

        /// <summary>
        /// Checks the validity of this transaction
        /// </summary>
        /// <returns>Flag indicating valid/not valid</returns>
        public bool CheckValidity()
        {
            if(Signature == null)
            {
                if (MinerReward != -1) return false;
                if (Value != Program.LocalBlockchain.MiningReward) return false;
                return true;
            }
            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(1024))
            {
                rsa.FromXmlString(FromAddress);
                if (rsa.VerifyData(Encoding.ASCII.GetBytes(FromAddress + " " + ToAddress + " " + TimeStamp + " " + Value + " " + MinerReward), new SHA1CryptoServiceProvider(), Signature))
                    return true;
                return false;
            }
        }
    }
}
