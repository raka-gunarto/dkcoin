﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;

namespace Blockchain_Proof_of_Concept
{
    [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
    public class Blockchain
    {
        #region Members

        public List<Block> Chain;
        public List<Transaction> PendingTransactions;
        public int Difficulty; // number of zeroes the hash should start with
        public float MiningReward;
        public int TargetTimeBetweenBlocks; // in seconds
        public int DecimalPlaces; // max denomination

        #endregion

        #region Constructors

        /// <summary>
        /// Blockchain Class.
        /// Used to represent and store a blockchain, JSON compatible.
        /// </summary>
        /// <param name="diff">Difficulty, number of minimum zeroes required for the hash</param>
        /// <param name="reward">Mining Reward, how much the miner gets for mining a block</param>
        /// <param name="targetTime">Target Block Time, target time between blocks</param>
        /// <param name="decPlaces">Max Decimal Places, maximum subdivision of the "currency"</param>
        public Blockchain(int diff, float reward,
            int targetTime, int decPlaces)
        {
            Chain = new List<Block>
            {

                GenesisBlock() // create the first block
            };
            Chain[0].MineBlock(2); // generate the hash for the first block 
            PendingTransactions = new List<Transaction>();
            Difficulty = diff;
            MiningReward = reward;
            TargetTimeBetweenBlocks = targetTime;
            DecimalPlaces = decPlaces;
        }

        #endregion

        #region Methods

        #region Block Related Methods

        /// <summary>
        /// Creates and returns the first block in the chain
        /// </summary>
        /// <returns>first "Genesis" block</returns>
        private Block GenesisBlock()
        {
            return new Block("", "", new List<Transaction>(),
                0,
                0, 0);
        }

        /// <summary>
        /// Gets the latest block in the chain, readonly
        /// </summary>
        [JsonIgnore]
        public Block LatestBlock
        {
            get { return Chain[Chain.Count - 1]; }
        }

        /// <summary>
        /// Adds a block to the chain.
        /// </summary>
        /// <param name="b">Block to add</param>
        public void AddBlock(Block b)
        {
            b.PreviousHash = LatestBlock.CurrentHash; // set the previous block hash
            Chain.Add(b);
        }

        /// <summary>
        /// Calculates the difficulty for the specified block ID
        /// </summary>
        /// <param name="blockNumber">block ID or number</param>
        /// <returns></returns>
        public int CalculateDifficulty(int blockNumber)
        {
            int difficulty = 2;
            if (blockNumber < 3) return 2; // first 3 blocks all have a difficulty of 2
            for (int i = 3; i < Chain.Count; i++)
            {
                Block prevBlock = Chain[i - 1];
                Block prev2Block = Chain[i - 2];

                if (Math.Abs(prevBlock.TimeStamp - prev2Block.TimeStamp) < TargetTimeBetweenBlocks
                ) // adjust difficulty to hit desired time per block
                    difficulty += 1;
                else
                    difficulty -= 1;

                if (difficulty < 1) difficulty = 1;
            }

            return difficulty;
        }

        #endregion

        /// <summary>
        /// Gets the balance for the specified address
        /// </summary>
        /// <param name="address">RSA public key</param>
        /// <returns>Balance of the specified address</returns>
        public float GetAddressBalance(string address)
        {
            float balance = 0;
            foreach (Block b in Chain)
            {
                foreach (Transaction tx in b.Transactions) // go through all transactions in the blockchain
                {
                    if (tx.FromAddress == address) balance -= tx.Value; // this address is sending, deduct from balance
                    if (tx.ToAddress == address) balance += tx.Value; // this address is receiving, add to balance
                }
            }

            return balance;
        }

        /// <summary>
        /// Checks if the blockchain is still valid or not
        /// </summary>
        /// <returns>Validity flag</returns>
        public bool ValidityCheck()
        {

            int difficulty = 2;
            for (int i = 1; i < Chain.Count; i++)
            {
                Block currentBlock = Chain[i];
                Block previousBlock = Chain[i - 1];

                if (currentBlock.CurrentHash !=
                    UtilityFunctions.CalculateSHA256Hash(JsonConvert.SerializeObject(currentBlock))
                ) // block's data has been changed
                    return false;

                if (currentBlock.PreviousHash != previousBlock.CurrentHash
                ) // this block / previous block's data has been changed
                    return false;

                if (i >= 3) // calculate the difficulty that should be used
                {
                    Block prevBlock = Chain[i - 1];
                    Block prev2Block = Chain[i - 2];

                    if (Math.Abs(prevBlock.TimeStamp - prev2Block.TimeStamp) < TargetTimeBetweenBlocks)
                        difficulty += 1;
                    else
                        difficulty -= 1;

                    if (difficulty < 1) difficulty = 1;
                }

                string leadingZeroes = "";
                for (int j = 0; j < difficulty; j++) leadingZeroes += "0";

                if (currentBlock.CalculateHash(false).Substring(0, difficulty) != leadingZeroes)
                    return false; // block hash valid for this difficulty?

                int coinbaseTransactions = 0;
                foreach (Transaction tx in currentBlock.Transactions)
                {
                    if (!tx.CheckValidity()) return false; // invalid tx
                    if (tx.MinerReward == -1)
                    {
                        if (tx.Value != Program.LocalBlockchain.MiningReward) return false; // too much reward
                        coinbaseTransactions++;
                    }

                    if (coinbaseTransactions > 1) return false; // more than one coinbase tx

                }

                if (coinbaseTransactions < 1) return false; // no coinbase tx
            }

            Difficulty = difficulty;
            return true;
        }

        #endregion
    }

    public class Block
    {
        #region Members

        public int ID;
        public string PreviousHash;
        [JsonIgnore] public string CurrentHash;
        public List<Transaction> Transactions;
        public int TimeStamp;
        public long Nonce;

        [JsonIgnore] public string JsonString => JsonConvert.SerializeObject(this);

        #endregion

        #region Constructors

        /// <summary>
        /// Block class.
        /// Used to represent and store a single block, JSON compatible.
        /// </summary>
        /// <param name="pHash">hash of the previous block in the chain</param>
        /// <param name="cHash">current hash of the block</param>
        /// <param name="allTransactions">All the transactions contained in this block</param>
        /// <param name="tStamp">UNIX timestamp of when this block was created</param>
        /// <param name="blockNonce">nonce generated from mining</param>
        /// <param name="id">Block ID</param>
        public Block(string pHash, string cHash,
            List<Transaction> allTransactions,
            int tStamp, int blockNonce, int id)
        {
            PreviousHash = pHash;
            CurrentHash = cHash;
            Transactions = allTransactions;
            TimeStamp = tStamp;
            Nonce = blockNonce;
            ID = id;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Calculates the hash of the block given all its data
        /// </summary>
        /// <param name="set">Flag to auto-set current hash to the output of this function</param>
        /// <returns></returns>
        public string CalculateHash(bool set)
        {
            string hash = UtilityFunctions.CalculateSHA256Hash(JsonString);
            if (set) // auto-set hash?
                CurrentHash = hash;
            return hash;
        }

        /// <summary>
        /// Mines this block with a given difficulty
        /// </summary>
        /// <param name="difficulty">block difficulty (number of minimum zeroes the has must start with)</param>
        /// <returns></returns>
        public bool MineBlock(int difficulty)
        {
            // Increment the nonce of this block until a hash with a number
            // of leading zeroes equal to the Chain's difficulty is produced.
            string hash;

            // generate prefix string to check
            string leadingZeroes = "";
            for (int i = 0; i < difficulty; i++)
                leadingZeroes += "0";

            Program.IsMining = true;
            do
            {
                string toHash = JsonString;
                hash = UtilityFunctions.CalculateSHA256Hash(toHash);
                if (hash.Substring(0, difficulty) != leadingZeroes) Nonce++;

                if (Program.MiningReset) // check if we should reset
                {
                    // check new bc
                    bool foundMatching = false;
                    foreach (Block b in Program.LocalBlockchain.Chain)
                    {
                        foreach (Transaction tx in b.Transactions)
                        {
                            if (Transactions.Contains(tx)) // found a processed tx that we are processing
                            {
                                Program.IsMining = false;
                                Program.MiningReset = false;

                                Transactions.Remove(tx);
                                foundMatching = true;
                            }
                        }
                    }

                    if (foundMatching)
                    {
                        // return non matching tx
                        Program.LocalBlockchain.PendingTransactions.AddRange(Transactions);
                        return false;
                    }
                }

                Program.MiningReset = false; // didn't find any clashes, we're ok
            } while (hash.Substring(0, difficulty) != leadingZeroes && Program.IsMiner);

            if (!Program.IsMiner && this.ID != 0)
            {
                Transaction coinbaseTransaction = null;
                foreach (Transaction tx in Transactions)
                {
                    if (tx.FromAddress == "") coinbaseTransaction = tx;
                }

                Transactions.Remove(coinbaseTransaction);
                if (Transactions != null && Program.LocalBlockchain != null)
                    Program.LocalBlockchain.PendingTransactions.AddRange(Transactions);

                return false;
            }

            Program.IsMining = false;
            CurrentHash = hash;
            if (ID != 0)
            {
                TimeStamp = UtilityFunctions.GetUnixTime(); // don't set timestamp in genesis block


                Program.MainGuiWindow.UpdateBalance(
                    Program.LocalBlockchain.GetAddressBalance(Program.RSAProvider.ToXmlString((false))));
            }

            return true;
        }

        #endregion
    }
}
