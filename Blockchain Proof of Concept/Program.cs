﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Msagl.Drawing;
using Newtonsoft.Json;

namespace Blockchain_Proof_of_Concept
{
    partial class Program
    {
        public static Blockchain LocalBlockchain;

        public static BlockchainNetworkListener Listener;
        public static bool ListenForConnections = true;

        // mining flags
        public static bool IsMiner = false;
        public static bool IsMining = false;
        public static bool MiningReset;

        // our rsa keypair
        public static RSACryptoServiceProvider RSAProvider;

        // port configs, should be the same, different for local testing purposes
        public static readonly int PORT = 7777;
        public static readonly int SERVER_PORT = 7777;

        // windows.forms
        public static MainGUI MainGuiWindow;
        public static NetworkDiscovery DiscoveryWindow;

        /// <summary>
        /// Runs BFS algorith to discover nodes in the network, given a starting node IP
        /// </summary>
        /// <param name="initialIP">The IP address of the seed node to query</param>
        /// <returns>A Tuple with the IP address of a node and its block height</returns>
        public static Tuple<IPAddress, int> NodeDiscover(IPAddress initialIP)
        {
            Thread gThread = new Thread(StartGraphViewer);
            gThread.Start();
            Thread.Sleep(1000); // wait for window to start
            Graph networkGraph = new Graph();

            // add initial node
            List<IPAddress> queue = new List<IPAddress> { initialIP };
            IPAddress IPWithLongestChain = null;
            int longestblockheight = LocalBlockchain.Chain.Count;
            

            // run bfs on the network
            while (queue.Count > 0)
            {
                IPAddress queryAddr = queue.First();
                queue.RemoveAt(0);

                Task<Tuple<List<IPAddress>, int>> queryTask = BlockchainNetworkClient.DiscoverRequest(queryAddr);
                queryTask.Wait();

                if (queryTask.Result == null) continue;
                networkGraph.AddEdge("127.0.0.1", "", queryAddr.ToString());
                DiscoveryWindow.UpdateGraph(networkGraph);

                BlockchainNetworkClient.Peers.Add(queryAddr);
                foreach (IPAddress addr in queryTask.Result.Item1)
                {
                    if (addr.ToString() == IPAddress.Loopback.ToString()) continue;
                    var host = Dns.GetHostEntry(Dns.GetHostName());

                    bool myaddress = false;
                    foreach (IPAddress ip in host.AddressList)
                    {
                        if (ip.AddressFamily == AddressFamily.InterNetwork)
                        {
                            if (addr.ToString() == ip.ToString()) myaddress = true;
                        }
                    }
                    if (myaddress) { 
                        networkGraph.AddEdge(queryAddr.ToString(), "127.0.0.1");     
                        DiscoveryWindow.UpdateGraph(networkGraph);
                        continue;
                    }

                    if (BlockchainNetworkClient.Peers.Contains(addr)) continue;
                    networkGraph.AddEdge(queryAddr.ToString(), addr.ToString());
                    DiscoveryWindow.UpdateGraph(networkGraph);
                    BlockchainNetworkClient.Peers.Add(addr);
                    queue.Add(addr);
                }

                if (queryTask.Result.Item2 > longestblockheight)
                {
                    IPWithLongestChain = queryAddr;
                    longestblockheight = queryTask.Result.Item2;
                }
            }
            DiscoveryWindow.Close();
            gThread.Join();
            return new Tuple<IPAddress, int>(IPWithLongestChain, longestblockheight);
        }

        /// <summary>
        /// Resolves blockchain conflict with a given node and its blockheight
        /// </summary>
        /// <param name="updateNode">A tuple with the node IP address to resolve with and their blockheight</param>
        public static void ConflictResolve(Tuple<IPAddress, int> updateNode)
        {
            bool updateSuccess = false; // blockchain compatibility
            while (LocalBlockchain.Chain.Count != updateNode.Item2)
            {
                Task<Block> blockTask = BlockchainNetworkClient.BlockRequest(updateNode.Item1, LocalBlockchain.Chain.Count);
                blockTask.Wait();

                Block recvBlock = blockTask.Result;
                recvBlock.CalculateHash(true);

                LocalBlockchain.Chain.Add(recvBlock); // try adding the block to our Chain
                if (!LocalBlockchain.ValidityCheck()) // invalid block!
                {
                    LocalBlockchain.Chain.Remove(LocalBlockchain.Chain.Last());
                    break;
                }
            }
            if (LocalBlockchain.Chain.Count == updateNode.Item2) updateSuccess = true;

            if (!updateSuccess)
            {
                Task<Blockchain> blocksTask = BlockchainNetworkClient.BlockchainRequest(updateNode.Item1);
                blocksTask.Wait();

                Blockchain newBlockchain = blocksTask.Result;
                if (newBlockchain.ValidityCheck()) LocalBlockchain = newBlockchain;
            }
        }

        /// <summary>
        /// Initial startup sync with the network
        /// </summary>
        /// <param name="initialIP">The IP address of the seed node to query</param>
        public static void NetworkSync(IPAddress initialIP)
        {
            // Discover other nodes 
            UtilityFunctions.Print("Discovering Nodes...", UtilityFunctions.PRINT_TYPE.INFO);
            Tuple<IPAddress, int> updateNode = NodeDiscover(initialIP);

            // Update our blockchain (if required)
            if (updateNode.Item1 != null)
            {
                UtilityFunctions.Print("Found updated blockchain, updating... ", UtilityFunctions.PRINT_TYPE.INFO);
                ConflictResolve(updateNode);
            }

            UtilityFunctions.Print("Registered and Synced With Network!", UtilityFunctions.PRINT_TYPE.INFO);
        }

        /// <summary>
        /// Event handler for when we receive a transaction broadcast
        /// </summary>
        /// <param name="tx">Broadcasted transaction</param>
        private static void Listener_TransactionBroadcastReceived(Transaction tx)
        {
            LocalBlockchain.PendingTransactions.Add(tx);
            UtilityFunctions.Print("Transaction Received:", UtilityFunctions.PRINT_TYPE.DEBUG);
            UtilityFunctions.Print("From: " + tx.FromAddress, UtilityFunctions.PRINT_TYPE.DEBUG);
            UtilityFunctions.Print("To: " + tx.ToAddress, UtilityFunctions.PRINT_TYPE.DEBUG);
            UtilityFunctions.Print("Value: " + tx.Value, UtilityFunctions.PRINT_TYPE.DEBUG);

            MainGuiWindow.UpdatePendingTransactions(LocalBlockchain.PendingTransactions);
        }

        /// <summary>
        /// Event handler for when we recieve a block broadcast
        /// </summary>
        /// <param name="addr">Address of the broadcaster</param>
        /// <param name="block">Broadcasted transaction</param>
        private static void Listener_BlockBroadcastReceived(IPAddress addr, Block block)
        {
            UtilityFunctions.Print("Block received from " + addr, UtilityFunctions.PRINT_TYPE.DEBUG);

            block.CalculateHash(true);
            if (block.ID < LocalBlockchain.Chain.Count) return;
            if (block.ID > LocalBlockchain.Chain.Count)
            {
                UtilityFunctions.Print("Block is further up in the Chain, resolving...", UtilityFunctions.PRINT_TYPE.DEBUG);
                ConflictResolve(new Tuple<IPAddress, int>(addr, block.ID + 1));
                return;
            }

            LocalBlockchain.Chain.Add(block);
            if (!LocalBlockchain.ValidityCheck())
            {
                LocalBlockchain.Chain.Remove(LocalBlockchain.Chain.Last());
                return;
            }
            UtilityFunctions.Print("Successfully added block to our Chain!", UtilityFunctions.PRINT_TYPE.DEBUG);
            // check our mining block
            if (IsMining)
            {
                MiningReset = true;
            }

            foreach(Block b in LocalBlockchain.Chain)
            {
                foreach(Transaction tx in b.Transactions)
                {
                    if (LocalBlockchain.PendingTransactions.Contains(tx)) LocalBlockchain.PendingTransactions.Remove(tx);
                }
            }

            MainGuiWindow.UpdatePendingTransactions(LocalBlockchain.PendingTransactions);
            MainGuiWindow.UpdateBalance(LocalBlockchain.GetAddressBalance(RSAProvider.ToXmlString((false))));
        }

        /// <summary>
        /// The mining loop
        /// </summary>
        /// <returns></returns>
        public static async Task MiningLoop()
        {
            while (IsMiner)
            {
                /* Make a new block with the pending transactions. */
                while (LocalBlockchain.PendingTransactions.Count == 0) await Task.Delay(5000);

                UtilityFunctions.Print("Pending transactions found, begin mining" , UtilityFunctions.PRINT_TYPE.INFO);
                Block newBlock = new Block(LocalBlockchain.LatestBlock.CurrentHash, "",
                                      new List<Transaction>(),
                                      UtilityFunctions.GetUnixTime(),
                                      0,
                                      LocalBlockchain.LatestBlock.ID + 1);
                while (newBlock.Transactions.Count <= 99 &&
                    LocalBlockchain.PendingTransactions.Count > 0)
                {
                    /* Add a transaction to the block. */
                    LocalBlockchain.PendingTransactions.Sort();

                    Transaction tx = (LocalBlockchain.PendingTransactions.First().MinerReward > LocalBlockchain.PendingTransactions.Last().MinerReward) ? LocalBlockchain.PendingTransactions.First() : LocalBlockchain.PendingTransactions.Last();

                    if (!tx.CheckValidity())
                    {
                        LocalBlockchain.PendingTransactions.Remove(tx);
                        continue;
                    }

                    // if it is valid, check sender address balance
                    if (LocalBlockchain.GetAddressBalance(tx.FromAddress) < tx.Value)
                    {
                        LocalBlockchain.PendingTransactions.Remove(tx);
                        continue;
                    }

                    newBlock.Transactions.Add(tx);
                    LocalBlockchain.PendingTransactions.Remove(tx);
                }

                Transaction coinbaseTx = new Transaction("", RSAProvider.ToXmlString(false), LocalBlockchain.MiningReward, -1);
                newBlock.Transactions.Add(coinbaseTx);

                int difficulty = LocalBlockchain.CalculateDifficulty(LocalBlockchain.Chain.Count); // new difficulty
                Task<bool> miningTask = Task.Run(() => newBlock.MineBlock(difficulty));
                miningTask.Wait();
                if (miningTask.Result)
                {
                    UtilityFunctions.Print("Mined a block", UtilityFunctions.PRINT_TYPE.INFO);

                    // mining completed, broadcast
                    UtilityFunctions.Print("Broadcasting block", UtilityFunctions.PRINT_TYPE.INFO);
                    BlockchainNetworkClient.BlockBroadcast(newBlock);

                    // update our bc
                    LocalBlockchain.Chain.Add(newBlock);
                    LocalBlockchain.Difficulty = difficulty;
                }

                MainGuiWindow.UpdatePendingTransactions(LocalBlockchain.PendingTransactions);
            }
        }

        /// <summary>
        /// Initial check for RSA keyfile, generates a keypair if one doesn't exist in the current directory
        /// </summary>
        public static void CheckRSAKey()
        {
            // check for keyfile
            if (File.Exists(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\keyfile.priv"))
            {
                StreamReader reader = new StreamReader(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"/keyfile.priv");
                string keypair = reader.ReadToEnd();

                RSAProvider = new RSACryptoServiceProvider(1024);
                RSAProvider.FromXmlString(keypair);
                reader.Close();
            }
            else
            {
                RSAProvider = new RSACryptoServiceProvider(1024);

                string keypair = RSAProvider.ToXmlString(true);
                StreamWriter writer = new StreamWriter(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"/keyfile.priv");
                writer.Write(keypair);

                string keypair2 = RSAProvider.ToXmlString(false);
                StreamWriter writer2 = new StreamWriter(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"/pubkey.public");
                writer2.Write(keypair2);

                writer.Close();
                writer2.Close();
            }
        }

        /// <summary>
        /// Initial check for a blockchain file, generates one if it doesn't exist
        /// </summary>
        public static void CheckBlockchainFile()
        {
            if (File.Exists(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"/Blockchain.json"))
            {
                StreamReader reader = new StreamReader(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"/Blockchain.json");
                string jsonstring = reader.ReadToEnd();
                LocalBlockchain = JsonConvert.DeserializeObject<Blockchain>(jsonstring);
                foreach (Block b in LocalBlockchain.Chain) b.CalculateHash(true);
                reader.Close();

                MainGuiWindow.UpdatePendingTransactions(LocalBlockchain.PendingTransactions);
                MainGuiWindow.UpdateBalance(LocalBlockchain.GetAddressBalance(RSAProvider.ToXmlString((false))));
            }
            else
            {
                LocalBlockchain = new Blockchain(2, 1.0f, 5, 8);
                MainGuiWindow.UpdatePendingTransactions(LocalBlockchain.PendingTransactions);
                MainGuiWindow.UpdateBalance(LocalBlockchain.GetAddressBalance(RSAProvider.ToXmlString((false))));
            }
        }

        [STAThread]
        public static void StartGraphViewer()
        {
            Application.EnableVisualStyles();

            DiscoveryWindow = new NetworkDiscovery();
            Application.Run(DiscoveryWindow);
        }

        [STAThread]
        public static void StartForm()
        {
            Application.EnableVisualStyles();

            MainGuiWindow = new MainGUI();
            Application.Run(MainGuiWindow);
        }

        static void Main(string[] args)
        {
            Thread winThread = new Thread(StartForm);
            winThread.Start();
            Thread.Sleep(1000); // wait for window to start

            BlockchainNetworkClient.Peers = new HashSet<IPAddress>();
            IPAddress seednode = IPAddress.Parse("127.0.0.1");

            if (args.Length == 1)
            {
                bool parseflag = IPAddress.TryParse(args[0], out seednode);

                if (!parseflag)
                {
                    Console.WriteLine("Usage: {0} [seed node IP]", AppDomain.CurrentDomain.FriendlyName);
                    return;
                }
            }
            CheckRSAKey();
            CheckBlockchainFile();

            Task.Run(WriteLoop);

            UtilityFunctions.Print("Registering and Synchronising With Network", UtilityFunctions.PRINT_TYPE.INFO);
            Task.Run(() => NetworkSync(seednode)).Wait();

            // Start the listener
            UtilityFunctions.Print("Starting Network Listener...", UtilityFunctions.PRINT_TYPE.DEBUG);
            Listener = new BlockchainNetworkListener();

            Task.Run(Listener.ListenLoop);
            Listener.BlockBroadcastReceived += Listener_BlockBroadcastReceived;
            Listener.TransactionBroadcastReceived += Listener_TransactionBroadcastReceived;

            //UserInputLoop(); this is now deprecated, MainGUI implemented instead
        }

        /// <summary>
        /// Writing loop to continuously save blockchain to disk
        /// </summary>
        /// <returns></returns>
        public static async Task WriteLoop()
        {
            while (true)
            {
                await Task.Delay(10000);
                StreamWriter writer = new StreamWriter(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"/Blockchain.json");
                writer.Write(JsonConvert.SerializeObject(LocalBlockchain));
                writer.Close();
            }
        }
    }

}
