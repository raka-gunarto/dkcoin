﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Blockchain_Proof_of_Concept
{
    partial class NetworkDiscovery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TopBar = new System.Windows.Forms.PictureBox();
            this.MinimiseButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TopBar)).BeginInit();
            this.SuspendLayout();
            // 
            // TopBar
            // 
            this.TopBar.BackColor = System.Drawing.Color.Transparent;
            this.TopBar.BackgroundImage = global::Blockchain_Proof_of_Concept.Properties.Resources.DKGraphToolbar;
            this.TopBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TopBar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TopBar.Location = new System.Drawing.Point(-1, -1);
            this.TopBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TopBar.Name = "TopBar";
            this.TopBar.Size = new System.Drawing.Size(878, 54);
            this.TopBar.TabIndex = 4;
            this.TopBar.TabStop = false;
            this.TopBar.Click += new System.EventHandler(this.TopBar_Click);
            this.TopBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TopBar_MouseDown);
            this.TopBar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TopBar_MouseMove);
            this.TopBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TopBar_MouseUp);
            // 
            // MinimiseButton
            // 
            this.MinimiseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.MinimiseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimiseButton.Font = new System.Drawing.Font("Impact", 10F);
            this.MinimiseButton.ForeColor = System.Drawing.Color.Gray;
            this.MinimiseButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.MinimiseButton.Location = new System.Drawing.Point(818, 12);
            this.MinimiseButton.Name = "MinimiseButton";
            this.MinimiseButton.Size = new System.Drawing.Size(36, 31);
            this.MinimiseButton.TabIndex = 5;
            this.MinimiseButton.Text = "_";
            this.MinimiseButton.UseVisualStyleBackColor = false;
            this.MinimiseButton.Click += new System.EventHandler(this.MinimiseButton_Click);
            // 
            // NetworkDiscovery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(866, 458);
            this.Controls.Add(this.MinimiseButton);
            this.Controls.Add(this.TopBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NetworkDiscovery";
            this.Text = "NetworkDiscovery";
            ((System.ComponentModel.ISupportInitialize)(this.TopBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBox TopBar;
        private Button MinimiseButton;
    }
}