﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Blockchain_Proof_of_Concept
{
    [SuppressMessage("ReSharper", "EmptyEmbeddedStatement")]
    public static class BlockchainNetworkClient
    {
        public static HashSet<IPAddress> Peers;
        #region Constants
        // ReSharper disable InconsistentNaming
        /// <summary>
        /// EOF byte, signals end of transmission
        /// </summary>
        public const byte END_OF_DATA = 4;

        /// <summary>
        /// ACK byte, signals data received
        /// </summary>
        public const byte ACK = 6;

        /// <summary>
        /// ERR byte, signals error
        /// </summary>
        public const byte ERROR = 0;
        // ReSharper restore InconsistentNaming
        #endregion

        #region NetResponses
        /// <summary>
        /// Sends a block with the given ID to a node in the network
        /// </summary>
        /// <param name="client">client node receiving a block</param>
        /// <param name="id">ID of the block</param>
        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public static void SendBlock(TcpClient client, int id)
        {
            NetworkStream stream = client.GetStream();

            if (id < 0 || id >= Program.LocalBlockchain.Chain.Count) // invalid block ID
            {
                stream.WriteByte(ERROR); // send error

                while (stream.ReadByte() != ACK) ;

                client.Close();
                client.Dispose();
                return;
            }

            byte[] data = UtilityFunctions.ConvertObjectToBytes(Program.LocalBlockchain.Chain[id]); // get bytes of the block

            // write to stream
            stream.Write(data, 0, data.Length);

            // wait for ack
            stream.ReadTimeout = 10000; // gives 10 seconds before timeout (one block is small, shouldn't take too long)
            while (stream.ReadByte() != ACK) ;
            client.Close();
            client.Dispose();
        }

        /// <summary>
        /// Sends a list of connected nodes to a node in the network
        /// </summary>
        /// <param name="client">receiving client node</param>
        /// <param name="nodesIP">list of IP addresses of nodes connected to this node</param>
        public static void SendNodes(TcpClient client, IEnumerable<IPAddress> nodesIP)
        {
            NetworkStream stream = client.GetStream();

            List<string> nodes = new List<string>();
            foreach (IPAddress addr in nodesIP) // convert IPAddresses to string
            {
                nodes.Add(addr.ToString()); 
            }

            byte[] data = UtilityFunctions.ConvertObjectToBytes(nodes);
            List<byte> bytes = data.ToList();

            bytes.AddRange(Encoding.ASCII.GetBytes(Program.LocalBlockchain.Chain.Count.ToString())); // add block height
            bytes.Add(END_OF_DATA);

            data = bytes.ToArray();

            // write to stream
            stream.Write(data, 0, data.Length);

            // wait for ack
            while (stream.ReadByte() != ACK) ;
            client.Close();
            client.Dispose();
        }

        /// <summary>
        /// Sends the whole blockchain to a node in the network
        /// </summary>
        /// <param name="client">receiving client node</param>
        public static void SendBlockchain(TcpClient client)
        {
            NetworkStream stream = client.GetStream();

            byte[] data = UtilityFunctions.ConvertObjectToBytes(Program.LocalBlockchain); // get bytes of blockchain

            // write to stream
            stream.Write(data, 0, data.Length);

            // wait for ack
            while (stream.ReadByte() != ACK) ;
            client.Close();
            client.Dispose();
        }
        #endregion

        #region NetRequests
        /// <summary>
        /// Sends a query for nodes in the network to a node
        /// </summary>
        /// <param name="remote">IP address of the node to query</param>
        /// <returns>A list of IP addresses of nodes in the network</returns>
        public static async Task<Tuple<List<IPAddress>, int>> DiscoverRequest(IPAddress remote)
        {
            TcpClient client = new TcpClient();

            try
            {
                await client.ConnectAsync(remote, Program.PORT);
            }
            catch (Exception connectionException) // connection refused
            {
                UtilityFunctions.Print(connectionException.Message, UtilityFunctions.PRINT_TYPE.ERROR);
                return null;
            }

            if (client.Connected)
            {
                NetworkStream stream = client.GetStream();
                BlockchainNetworkPacket packet = new BlockchainNetworkPacket
                {
                    flag = BlockchainNetworkPacket.TYPE_FLAGS.DISCOVER // we want to find other nodes
                };

                byte[] data = UtilityFunctions.ConvertObjectToBytes(packet);

                stream.Write(data, 0, data.Length);

                List<byte> bytes = new List<byte>();

                // read peers
                while (true) 
                {
                    int b = stream.ReadByte();
                    if (b == END_OF_DATA) break;
                    bytes.Add((byte)b);
                }
                String ipJsonString = UtilityFunctions.BytesToString(bytes.ToArray());

                bytes.Clear();

                // read blockheight
                while (true)
                {
                    int b = stream.ReadByte();
                    if (b == END_OF_DATA) break;
                    bytes.Add((byte)b);
                }
                String intString = UtilityFunctions.BytesToString(bytes.ToArray());

                stream.WriteByte(ACK); // send ACK

                await Task.Delay(50);
                client.Close();
                client.Dispose();

                // convert text IPAddresses to IPAddress objects
                List<IPAddress> addresses = new List<IPAddress>();
                foreach (string ipstring in JsonConvert.DeserializeObject<List<string>>(ipJsonString))
                {
                    addresses.Add(IPAddress.Parse(ipstring));
                }
                return Tuple.Create(addresses, int.Parse(intString));
            }

            throw new ArgumentException("IP invalid", "remote");
        }

        /// <summary>
        /// Sends a query for a node's whole blockchain
        /// </summary>
        /// <param name="remote">IP address of the node to query</param>
        /// <returns>Blockchain</returns>
        public static async Task<Blockchain> BlockchainRequest(IPAddress remote)
        {
            TcpClient client = new TcpClient();
            await client.ConnectAsync(remote, Program.PORT);

            if (client.Connected)
            {
                NetworkStream stream = client.GetStream();
                BlockchainNetworkPacket packet = new BlockchainNetworkPacket
                {
                    flag = BlockchainNetworkPacket.TYPE_FLAGS.FULLDATA_REQ // requesting whole blockchain
                };

                byte[] data = UtilityFunctions.ConvertObjectToBytes(packet);

                stream.Write(data, 0, data.Length);

                List<byte> bytes = new List<byte>();
                // read blockchain data
                while (true)
                {
                    int b = stream.ReadByte();
                    if (b == END_OF_DATA) break;
                    bytes.Add((byte)b);
                }
                String jsonString = UtilityFunctions.BytesToString(bytes.ToArray());
                stream.WriteByte(ACK); // send ACK

                await Task.Delay(50);
                client.Close();
                client.Dispose();

                Blockchain bc = JsonConvert.DeserializeObject<Blockchain>(jsonString);

                // calculate hashes
                foreach (Block b in bc.Chain)
                {
                    b.CalculateHash(true);
                }
                return bc;
            }

            throw new ArgumentException("IP invalid", "remote");
        }

        /// <summary>
        /// Requests a block with a given ID from a node in the network
        /// </summary>
        /// <param name="remote">IP Address of the node to query</param>
        /// <param name="id">ID of the block to send</param>
        /// <returns>A block</returns>
        public static async Task<Block> BlockRequest(IPAddress remote, int id)
        {
            TcpClient client = new TcpClient();
            await client.ConnectAsync(remote, Program.PORT);

            if (client.Connected)
            {
                NetworkStream stream = client.GetStream();
                BlockchainNetworkPacket packet = new BlockchainNetworkPacket
                {
                    flag = BlockchainNetworkPacket.TYPE_FLAGS.DATA_REQ, // we want to get a block
                    BlockIDRequest = id // block id of requested block
                };

                byte[] data = UtilityFunctions.ConvertObjectToBytes(packet);

                stream.Write(data, 0, data.Length);

                List<byte> bytes = new List<byte>();

                // read block data
                while (true)
                {
                    int b = stream.ReadByte();
                    if (b == ERROR) throw new ArgumentException("ID out of range", "id");
                    if (b == END_OF_DATA) break;
                    bytes.Add((byte)b);
                }
                String jsonString = UtilityFunctions.BytesToString(bytes.ToArray());
                stream.WriteByte(ACK); // send ACK

                await Task.Delay(50);
                client.Close();
                client.Dispose();
                return JsonConvert.DeserializeObject<Block>(jsonString);
            }

            throw new ArgumentException("IP invalid", "remote");
        }
        #endregion

        #region Broadcasts
        /// <summary>
        /// Broadcasts the given transaction to all connected nodes
        /// </summary>
        /// <param name="tx">Transaction to broadcast</param>
        public static void TransactionBroadcast(Transaction tx)
        {
            // send to all peers
            foreach (IPAddress ip in Peers)
            {
                Task.Run(async () =>
                {
                    TcpClient client = new TcpClient();
                    await client.ConnectAsync(ip, Program.PORT);

                    if (client.Connected)
                    {
                        BlockchainNetworkPacket packet = new BlockchainNetworkPacket
                        {
                            flag = BlockchainNetworkPacket.TYPE_FLAGS.TX_BROAD, // this is a broadcast
                            Tx = tx // transaction data
                        };

                        byte[] data = UtilityFunctions.ConvertObjectToBytes(packet);
                        client.GetStream().Write(data, 0, data.Length);

                        while (client.GetStream().ReadByte() != ACK) ;
                        client.Close();
                        client.Dispose();
                    }
                });
            }
        }

        /// <summary>
        /// Broadcasts given block to all nodes in the network
        /// </summary>
        /// <param name="b">Block to broadcast</param>
        public static void BlockBroadcast(Block b)
        {
            // broadcast block to all peers
            foreach (IPAddress ip in Peers)
            {
                Task.Run(async () =>
                {
                    TcpClient client = new TcpClient();
                    await client.ConnectAsync(ip, Program.PORT);

                    if (client.Connected)
                    {
                        BlockchainNetworkPacket packet = new BlockchainNetworkPacket
                        {
                            flag = BlockchainNetworkPacket.TYPE_FLAGS.BLOCK_BROAD, // this is a block broadcast
                            Block = b // block data
                        };

                        byte[] data = UtilityFunctions.ConvertObjectToBytes(packet);
                        client.GetStream().Write(data, 0, data.Length);

                        while (client.GetStream().ReadByte() != ACK) ;
                        client.Close();
                        client.Dispose();
                    }
                });
            }
        }

        #endregion
    }
}
