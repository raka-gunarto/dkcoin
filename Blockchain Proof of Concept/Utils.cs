﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace Blockchain_Proof_of_Concept
{
    /// <summary>
    /// Static utility functions for ease-of-life
    /// </summary>
    public static class UtilityFunctions
    {
        public static bool DebugFlag = true;

        public enum PRINT_TYPE { INFO, WARN, ERROR, DEBUG }
        private static Dictionary<PRINT_TYPE, string> printPrefixes = new Dictionary<PRINT_TYPE, string>
        {
            { PRINT_TYPE.INFO, "[*] "},
            { PRINT_TYPE.WARN, "[!] "},
            { PRINT_TYPE.ERROR, "[!!!] "},
            { PRINT_TYPE.DEBUG, "[...] "}
        };

        /// <summary>
        /// Prints a message to the console with a prefix based on the message type
        /// </summary>
        /// <param name="message">Message to print</param>
        /// <param name="type">Type of message (specifies the prefix)</param>
        public static void Print(string message, PRINT_TYPE type)
        {
            if (!DebugFlag && type == PRINT_TYPE.DEBUG) return;
            Console.WriteLine(printPrefixes[type] + message);
        }

        /// <summary>
        /// Gets the current UNIX time
        /// </summary>
        /// <returns></returns>
        public static int GetUnixTime()
        {
            return (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        /// <summary>
        /// Converts ASCII bytes to string
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string BytesToString(byte[] bytes)
        {
            return Encoding.ASCII.GetString(bytes);
        }

        /// <summary>
        /// Calculates the SHA256 hash of a string and returns a string as well
        /// </summary>
        /// <param name="s">String to hash</param>
        /// <returns>Hash of the specified string</returns>
        public static string CalculateSHA256Hash(string s)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                // By default, ComputeHash returns the hashed
                // data as a byte array - we need to convert it
                // into a string.
                byte[] b = sha256.ComputeHash(Encoding.ASCII.GetBytes(s));
                string hash = String.Empty;
                foreach (byte theByte in b)
                {
                    hash += theByte.ToString("x2"); // it's in binary
                }
                return hash;
            }
        }

        /// <summary>
        /// Converts an object to bytes for transmission, adds the EOF byte as well
        /// </summary>
        /// <param name="o">Object to convert</param>
        /// <returns>Byte array suitable for transmission</returns>
        public static byte[] ConvertObjectToBytes(object o)
        {
            // convert object to bytes
            String packetstring = JsonConvert.SerializeObject(o);
            byte[] data = Encoding.ASCII.GetBytes(packetstring);

            // add null for specifying end of text
            List<byte> bytes = data.ToList();
            bytes.Add(BlockchainNetworkClient.END_OF_DATA);

            return bytes.ToArray<byte>();
        }
    }
}
