﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Msagl.Drawing;
using Microsoft.Msagl.GraphViewerGdi;
using Color = System.Drawing.Color;

namespace Blockchain_Proof_of_Concept
{
    public partial class NetworkDiscovery : Form
    {
        GViewer graphViewer = new GViewer();
        Graph networkGraph = new Graph();

        public NetworkDiscovery()
        {
            InitializeComponent();

            graphViewer.Dock = DockStyle.Fill;
            graphViewer.Graph = networkGraph;

            graphViewer.BackColor = Color.FromArgb(0, 30, 30, 30);
            graphViewer.ForeColor = Color.FromArgb(0, 30, 30, 30);
            graphViewer.ToolBarIsVisible = false;
            

            Controls.Add(graphViewer);
        }

        delegate void UpdateGraphCallback(Graph g);
        public void UpdateGraph(Graph g)
        {
            if (graphViewer.InvokeRequired) // thread safety
            {
                Invoke(new UpdateGraphCallback(UpdateGraph), g);
                return;
            }

            graphViewer.Graph = g;
        }

        delegate void CloseCallback(); // safe cross thread close
        public new void Close()
        {
            Invoke(new CloseCallback(base.Close));
        }

        /* Make a borderless window draggable. */
        private bool _isMouseDown;           /* Is the user holding the left mouse button down? */
        private Point _prevLocation;         /* Screen position of the last mouse event. */
        private void TopBar_MouseDown(object sender, MouseEventArgs e)
        {
            _isMouseDown = true;
            _prevLocation = e.Location;
        }

        private void TopBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isMouseDown)
            {
                Location = new Point((Location.X - _prevLocation.X) + e.X,
                    (Location.Y - _prevLocation.Y) + e.Y);
                Update();
            }
        }

        private void TopBar_MouseUp(object sender, MouseEventArgs e)
        {
            _isMouseDown = false;
        }

        private void TopBar_Click(object sender, EventArgs e)
        {

        }

        private void MinimiseButton_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
