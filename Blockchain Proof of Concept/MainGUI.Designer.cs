﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Blockchain_Proof_of_Concept
{
    partial class MainGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainGUI));
            this.CloseButton = new System.Windows.Forms.Button();
            this.MinimiseButton = new System.Windows.Forms.Button();
            this.PendingTransactionList = new System.Windows.Forms.ListBox();
            this.PendingTransactionsTitleLabel = new System.Windows.Forms.Label();
            this.BalanceLabel = new System.Windows.Forms.Label();
            this.TransactionHistoryBox = new System.Windows.Forms.ListBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.BalanceLabelOther = new System.Windows.Forms.Label();
            this.BlockchainExplorerTitle = new System.Windows.Forms.Label();
            this.BlockchainExplorer = new System.Windows.Forms.ListBox();
            this.BlockchainIDInput = new System.Windows.Forms.TextBox();
            this.MakeTransactionTitle = new System.Windows.Forms.Label();
            this.TransactionTargetAddressInput = new System.Windows.Forms.TextBox();
            this.TransactionAmountInput = new System.Windows.Forms.TextBox();
            this.ClearTransactionButton = new System.Windows.Forms.Button();
            this.SendTransactionButton = new System.Windows.Forms.Button();
            this.MinerTabButton = new System.Windows.Forms.Button();
            this.WalletTabButton = new System.Windows.Forms.Button();
            this.TopBar = new System.Windows.Forms.PictureBox();
            this.MineButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TopBar)).BeginInit();
            this.SuspendLayout();
            // 
            // CloseButton
            // 
            this.CloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            resources.ApplyResources(this.CloseButton, "CloseButton");
            this.CloseButton.ForeColor = System.Drawing.Color.Gray;
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.UseVisualStyleBackColor = false;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // MinimiseButton
            // 
            this.MinimiseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            resources.ApplyResources(this.MinimiseButton, "MinimiseButton");
            this.MinimiseButton.ForeColor = System.Drawing.Color.Gray;
            this.MinimiseButton.Name = "MinimiseButton";
            this.MinimiseButton.UseVisualStyleBackColor = false;
            this.MinimiseButton.Click += new System.EventHandler(this.MinimiseButton_Click);
            // 
            // PendingTransactionList
            // 
            this.PendingTransactionList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.PendingTransactionList.ForeColor = System.Drawing.SystemColors.Window;
            this.PendingTransactionList.FormattingEnabled = true;
            resources.ApplyResources(this.PendingTransactionList, "PendingTransactionList");
            this.PendingTransactionList.Name = "PendingTransactionList";
            this.PendingTransactionList.Tag = "MINER";
            // 
            // PendingTransactionsTitleLabel
            // 
            resources.ApplyResources(this.PendingTransactionsTitleLabel, "PendingTransactionsTitleLabel");
            this.PendingTransactionsTitleLabel.ForeColor = System.Drawing.Color.Aqua;
            this.PendingTransactionsTitleLabel.Name = "PendingTransactionsTitleLabel";
            this.PendingTransactionsTitleLabel.Tag = "MINER";
            // 
            // BalanceLabel
            // 
            resources.ApplyResources(this.BalanceLabel, "BalanceLabel");
            this.BalanceLabel.BackColor = System.Drawing.Color.Transparent;
            this.BalanceLabel.ForeColor = System.Drawing.Color.Aqua;
            this.BalanceLabel.Name = "BalanceLabel";
            this.BalanceLabel.Tag = "WALLET";
            // 
            // TransactionHistoryBox
            // 
            this.TransactionHistoryBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.TransactionHistoryBox.ForeColor = System.Drawing.SystemColors.Window;
            this.TransactionHistoryBox.FormattingEnabled = true;
            resources.ApplyResources(this.TransactionHistoryBox, "TransactionHistoryBox");
            this.TransactionHistoryBox.Name = "TransactionHistoryBox";
            this.TransactionHistoryBox.Tag = "WALLET";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(255)))), ((int)(((byte)(47)))));
            this.textBox1.Name = "textBox1";
            this.textBox1.Tag = "WALLET";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // BalanceLabelOther
            // 
            resources.ApplyResources(this.BalanceLabelOther, "BalanceLabelOther");
            this.BalanceLabelOther.BackColor = System.Drawing.Color.Transparent;
            this.BalanceLabelOther.ForeColor = System.Drawing.Color.Lime;
            this.BalanceLabelOther.Name = "BalanceLabelOther";
            this.BalanceLabelOther.Tag = "WALLET";
            // 
            // BlockchainExplorerTitle
            // 
            resources.ApplyResources(this.BlockchainExplorerTitle, "BlockchainExplorerTitle");
            this.BlockchainExplorerTitle.BackColor = System.Drawing.Color.Transparent;
            this.BlockchainExplorerTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(10)))), ((int)(((byte)(123)))));
            this.BlockchainExplorerTitle.Name = "BlockchainExplorerTitle";
            this.BlockchainExplorerTitle.Tag = "WALLET";
            // 
            // BlockchainExplorer
            // 
            this.BlockchainExplorer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.BlockchainExplorer.ForeColor = System.Drawing.SystemColors.Window;
            this.BlockchainExplorer.FormattingEnabled = true;
            resources.ApplyResources(this.BlockchainExplorer, "BlockchainExplorer");
            this.BlockchainExplorer.Name = "BlockchainExplorer";
            this.BlockchainExplorer.Tag = "WALLET";
            // 
            // BlockchainIDInput
            // 
            this.BlockchainIDInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            resources.ApplyResources(this.BlockchainIDInput, "BlockchainIDInput");
            this.BlockchainIDInput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(10)))), ((int)(((byte)(123)))));
            this.BlockchainIDInput.Name = "BlockchainIDInput";
            this.BlockchainIDInput.Tag = "WALLET";
            this.BlockchainIDInput.TextChanged += new System.EventHandler(this.BlockchainIDInput_TextChanged);
            // 
            // MakeTransactionTitle
            // 
            resources.ApplyResources(this.MakeTransactionTitle, "MakeTransactionTitle");
            this.MakeTransactionTitle.BackColor = System.Drawing.Color.Transparent;
            this.MakeTransactionTitle.ForeColor = System.Drawing.Color.Yellow;
            this.MakeTransactionTitle.Name = "MakeTransactionTitle";
            this.MakeTransactionTitle.Tag = "WALLET";
            // 
            // TransactionTargetAddressInput
            // 
            this.TransactionTargetAddressInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            resources.ApplyResources(this.TransactionTargetAddressInput, "TransactionTargetAddressInput");
            this.TransactionTargetAddressInput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.TransactionTargetAddressInput.Name = "TransactionTargetAddressInput";
            this.TransactionTargetAddressInput.Tag = "WALLET";
            // 
            // TransactionAmountInput
            // 
            this.TransactionAmountInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            resources.ApplyResources(this.TransactionAmountInput, "TransactionAmountInput");
            this.TransactionAmountInput.ForeColor = System.Drawing.Color.Yellow;
            this.TransactionAmountInput.Name = "TransactionAmountInput";
            this.TransactionAmountInput.Tag = "WALLET";
            // 
            // ClearTransactionButton
            // 
            this.ClearTransactionButton.BackgroundImage = global::Blockchain_Proof_of_Concept.Properties.Resources.DKGradient1;
            resources.ApplyResources(this.ClearTransactionButton, "ClearTransactionButton");
            this.ClearTransactionButton.ForeColor = System.Drawing.Color.Yellow;
            this.ClearTransactionButton.Name = "ClearTransactionButton";
            this.ClearTransactionButton.Tag = "WALLET";
            this.ClearTransactionButton.UseVisualStyleBackColor = true;
            this.ClearTransactionButton.Click += new System.EventHandler(this.ClearTransactionButton_Click);
            // 
            // SendTransactionButton
            // 
            this.SendTransactionButton.BackgroundImage = global::Blockchain_Proof_of_Concept.Properties.Resources.DKGradient1;
            resources.ApplyResources(this.SendTransactionButton, "SendTransactionButton");
            this.SendTransactionButton.ForeColor = System.Drawing.Color.Lime;
            this.SendTransactionButton.Name = "SendTransactionButton";
            this.SendTransactionButton.Tag = "WALLET";
            this.SendTransactionButton.UseVisualStyleBackColor = true;
            this.SendTransactionButton.Click += new System.EventHandler(this.SendTransactionButton_Click);
            // 
            // MinerTabButton
            // 
            this.MinerTabButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.MinerTabButton.BackgroundImage = global::Blockchain_Proof_of_Concept.Properties.Resources.MinerButtonV1PNGV2;
            resources.ApplyResources(this.MinerTabButton, "MinerTabButton");
            this.MinerTabButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.MinerTabButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.MinerTabButton.FlatAppearance.BorderSize = 0;
            this.MinerTabButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.MinerTabButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.MinerTabButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(145)))), ((int)(((byte)(145)))));
            this.MinerTabButton.Name = "MinerTabButton";
            this.MinerTabButton.UseVisualStyleBackColor = false;
            this.MinerTabButton.Click += new System.EventHandler(this.MinerTabButton_Click);
            this.MinerTabButton.MouseEnter += new System.EventHandler(this.MinerTabButton_Enter);
            this.MinerTabButton.MouseLeave += new System.EventHandler(this.MinerTabButton_Leave);
            // 
            // WalletTabButton
            // 
            this.WalletTabButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.WalletTabButton.BackgroundImage = global::Blockchain_Proof_of_Concept.Properties.Resources.WalletButtonPNGv1;
            resources.ApplyResources(this.WalletTabButton, "WalletTabButton");
            this.WalletTabButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.WalletTabButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.WalletTabButton.FlatAppearance.BorderSize = 0;
            this.WalletTabButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.WalletTabButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.WalletTabButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(145)))), ((int)(((byte)(145)))));
            this.WalletTabButton.Name = "WalletTabButton";
            this.WalletTabButton.UseVisualStyleBackColor = false;
            this.WalletTabButton.Click += new System.EventHandler(this.WalletTabButton_Click);
            this.WalletTabButton.MouseEnter += new System.EventHandler(this.WalletTabButton_Enter);
            this.WalletTabButton.MouseLeave += new System.EventHandler(this.WalletTabButton_Leave);
            // 
            // TopBar
            // 
            this.TopBar.BackColor = System.Drawing.Color.Transparent;
            this.TopBar.BackgroundImage = global::Blockchain_Proof_of_Concept.Properties.Resources.DKToolbarGradientFinalPNGV31;
            resources.ApplyResources(this.TopBar, "TopBar");
            this.TopBar.Name = "TopBar";
            this.TopBar.TabStop = false;
            this.TopBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GUI_MouseDown);
            this.TopBar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.GUI_MouseMove);
            this.TopBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.GUI_MouseUp);
            // 
            // MineButton
            // 
            this.MineButton.BackgroundImage = global::Blockchain_Proof_of_Concept.Properties.Resources.MinerPlayButton2;
            resources.ApplyResources(this.MineButton, "MineButton");
            this.MineButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MineButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.MineButton.FlatAppearance.BorderSize = 0;
            this.MineButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.MineButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.MineButton.Name = "MineButton";
            this.MineButton.Tag = "MINER";
            this.MineButton.UseVisualStyleBackColor = true;
            this.MineButton.Click += new System.EventHandler(this.MineButton_Click);
            this.MineButton.MouseEnter += new System.EventHandler(this.MineButtonEnter);
            this.MineButton.MouseLeave += new System.EventHandler(this.MineButtonLeave);
            // 
            // MainGUI
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.Controls.Add(this.ClearTransactionButton);
            this.Controls.Add(this.SendTransactionButton);
            this.Controls.Add(this.TransactionAmountInput);
            this.Controls.Add(this.TransactionTargetAddressInput);
            this.Controls.Add(this.MakeTransactionTitle);
            this.Controls.Add(this.BlockchainIDInput);
            this.Controls.Add(this.BlockchainExplorer);
            this.Controls.Add(this.BlockchainExplorerTitle);
            this.Controls.Add(this.BalanceLabelOther);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.TransactionHistoryBox);
            this.Controls.Add(this.BalanceLabel);
            this.Controls.Add(this.MinimiseButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.MinerTabButton);
            this.Controls.Add(this.WalletTabButton);
            this.Controls.Add(this.TopBar);
            this.Controls.Add(this.PendingTransactionsTitleLabel);
            this.Controls.Add(this.PendingTransactionList);
            this.Controls.Add(this.MineButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainGUI";
            this.Load += new System.EventHandler(this.GUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TopBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button CloseButton;
        private Button WalletTabButton;
        private Button MinerTabButton;
        private PictureBox TopBar;
        private Button MinimiseButton;
        private Button MineButton;
        private ListBox PendingTransactionList;
        private Label PendingTransactionsTitleLabel;
        private Label BalanceLabel;
        private ListBox TransactionHistoryBox;
        private TextBox textBox1;
        private Label BalanceLabelOther;
        private Label BlockchainExplorerTitle;
        private ListBox BlockchainExplorer;
        private TextBox BlockchainIDInput;
        private Label MakeTransactionTitle;
        private TextBox TransactionTargetAddressInput;
        private TextBox TransactionAmountInput;
        private Button SendTransactionButton;
        private Button ClearTransactionButton;
    }
}